const express = require("express");
const router = express.Router();
const controller = require("./controller/controller");

router.get("/check-health", controller.checkHealth);
router.get("/biodata", controller.biodata);
router.post("/biodata", controller.query);

module.exports = router;